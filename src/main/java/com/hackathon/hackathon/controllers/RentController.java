package com.hackathon.hackathon.controllers;

import com.hackathon.hackathon.models.RentModel;
import com.hackathon.hackathon.services.RentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hackaton")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class RentController {

    @Autowired
    RentService rentService;

    @GetMapping("/rents")
    public ResponseEntity<List<RentModel>> getRents() {
        System.out.println("RentController - getRents()");

        return new ResponseEntity<>(
                rentService.getRents(),
                HttpStatus.OK
        );
    }

    @PostMapping("/rents")
    public ResponseEntity<RentModel> addRent(@RequestBody RentModel rent) {

        System.out.println("RentController - addRent()");
        System.out.println("RentController - la id del purchase a crear es : "+ rent.getId());
        System.out.println("RentController - la userId del purchase a crear es : "+rent.getUserId());
        System.out.println("RentController - el filmId del purchase a crear es : "+rent.getFilmId());

        RentModel rentToAdd= rentService.addRent(rent);

        if(rentToAdd!=null) {
            return new ResponseEntity<>(
                    rentToAdd,
                    HttpStatus.CREATED
            );
        }else{
            return new ResponseEntity<>(
                    new RentModel(),
                    HttpStatus.NOT_FOUND
            );
        }
    }

    @DeleteMapping("/rents/{id}")
    public ResponseEntity deleteRent(@PathVariable String id){
        System.out.println("RentController - deleteRent()");
        System.out.println("RentController - la id del rent a eliminar es : "+ id);

        boolean deleteRent = rentService.deleteRent(id);

        return new ResponseEntity<>(
                deleteRent ? "Rent borrado" : "Rent no borrado",
                deleteRent ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
