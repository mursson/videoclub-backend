package com.hackathon.hackathon.services;

import com.hackathon.hackathon.models.ClientModel;
import com.hackathon.hackathon.models.FilmModel;
import com.hackathon.hackathon.models.RentModel;
import com.hackathon.hackathon.repositories.RentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RentService {

    @Autowired
    RentRepository rentRepository;

    @Autowired
    FilmService filmService;

    @Autowired
    ClientService clientService;

    public List<RentModel> getRents() {
        System.out.println("RentService - getRents()");

        return rentRepository.findAll();
    }

    public RentModel addRent(RentModel rent) {

        System.out.println("RentService - addRent()");
        System.out.println("RentService - id en purchase: "+rent.getId());
        System.out.println("RentService - userId en purchase: "+rent.getUserId());
        System.out.println("RentService - filmId en purchase: "+rent.getFilmId());

        //Valido client
        if(clientService.findById(rent.getUserId()).isEmpty()) {
            System.out.println("El usuario  no se ha encontrado");
            return null;
        }

        //Validamos id Rent
        if(this.findById(rent.getId()).isPresent()) {
            System.out.println("El idRent se ha encontrado, Ya existe una compra");
            return null;
        }

        //Validamos idFilm
        if(!filmService.findById(rent.getFilmId()).isPresent()){
            System.out.println("El film no se ha encontrado");
            return null;
        }

        int quantity = 0;
        Optional<FilmModel> film;

        quantity = filmService.findById(rent.getFilmId()).get().getQuantity();
        film = filmService.findById(rent.getFilmId());
        System.out.println("quantity : "+quantity);

        if(quantity>0) {
            System.out.println("Hay cantidad");
            //filmService.findById(rent.getFilmId()).get().setQuantity(filmService.findById(rent.getFilmId()).get().getQuantity()-1);
            film.get().setQuantity(quantity-1);
            filmService.update(film.get());
        } else {
            System.out.println("Cantidad de Film 0 - No se puede alquilar.");
            return null;
        }

        return rentRepository.save(rent);
    }

    public Optional<RentModel> findById(String id) {
        System.out.println("RentService - findById()");

        return rentRepository.findById(id);
    }

    public boolean deleteRent(String id) {
        System.out.println("RentService - deleteRent()");
        System.out.println("RentService - deleteRent() - rent a borrar es: "+id);

        boolean result= false;
        int quantity = 0;
        String idFilm;
        Optional<FilmModel> film;
        Optional<RentModel> rent;

        rent= this.findById(id);

        if (rent.isPresent()) {
            System.out.println("RentService - deleteRent() - rent encontrado, lo borramos");

            idFilm = rent.get().getFilmId();
            System.out.println("RentService - deleteRent() - idFilm: "+idFilm);

            quantity = filmService.findById(idFilm).get().getQuantity();
            film = filmService.findById(idFilm);
            System.out.println("quantity : "+quantity);

            if(quantity>=0) {
                System.out.println("Hay cantidad");
                //filmService.findById(rent.getFilmId()).get().setQuantity(filmService.findById(rent.getFilmId()).get().getQuantity()-1);
                film.get().setQuantity(quantity+1);
                filmService.update(film.get());

                rentRepository.deleteById(id);
                result = true;

            } else {
                System.out.println("Cantidad de Film 0 - No se puede devolver.");
                result= false;
            }

        }
        return result;
    }
}
