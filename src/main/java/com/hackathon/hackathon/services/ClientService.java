package com.hackathon.hackathon.services;

import com.hackathon.hackathon.models.ClientModel;
import com.hackathon.hackathon.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService {
    @Autowired
    ClientRepository clientRepository;
    public List findAll(String orderBy) {

        System.out.println("findAll en ClientService");
        List<ClientModel> res;
        if (orderBy!=null){
            System.out.println("Se ha pedido ordenación");
            res=this.clientRepository.findAll(Sort.by("age").descending());
        }
        else {
            res=this.clientRepository.findAll();
        }
        return res;
    }

    public Optional<ClientModel> findById(String id) {
        System.out.println("find by id en userservice");
        return this.clientRepository.findById(id);
    }

    public ClientModel addUser(ClientModel client) {
        System.out.println("addUser en ProductServices");

        return this.clientRepository.save(client);
    }

    public ClientModel update(ClientModel user) {
        System.out.println("update en userService");

        return this.clientRepository.save(user);
    }

    public boolean delete(String id) {
        boolean result = false;
        System.out.println("delete en ClientService");
        if (this.findById(id).isPresent() == true) {
            System.out.println("User encontrado, borrando");
            this.clientRepository.deleteById(id);
            result = true;
        } else
            System.out.println("User no encontrado");
        return result;
    }
}
