package com.hackathon.hackathon.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "films")
public class FilmModel {

    @Id
    private String id;
    private String desc;
    private float price;
    private int quantity;
    private String genre;
    private String photo;

    public FilmModel() {
    }

    public FilmModel(String id, String desc, float price, int quantity, String genre, String photo) {
        this.id = id;
        this.desc = desc;
        this.price = price;
        this.quantity = quantity;
        this.genre = genre;
        this.photo = photo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
