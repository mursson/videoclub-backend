package com.hackathon.hackathon.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document(collection = "rents")
public class RentModel {

    @Id
    private String id;
    private String userId;
    private String filmId;

    public RentModel() {
    }

    public RentModel(String id, String userId, String filmId) {
        this.id = id;
        this.userId = userId;
        this.filmId = filmId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFilmId() {
        return filmId;
    }

    public void setFilmId(String filmId) {
        this.filmId = filmId;
    }
}
