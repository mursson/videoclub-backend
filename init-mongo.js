db.createUser(
    {
        user: "api_user",
        pwd: "NdEep0XLpMNKUmgQVa81oDCx7mrSRodh0Z79qdX3",
        roles:[
            {
                role: "readWrite",
                db:"hackathon"
            }
        ]
    }
);

db.createCollection('clients');
db.clients.insertMany(
[
    {
        "id": "1",
        "name": "Fernando",
        "age": 22
    },
    {
        "id": "2",
        "name": "Lin",
        "age": 25
    },
    {
        "id": "3",
        "name": "Antonio",
        "age": 41
    }
]
);

db.createCollection('films');

db.films.insertMany(
[
    {
        "id": "2",
        "desc": "Jurassic Park",
        "price": 5.0,
        "quantity": 2,
        "genre": "Thriller",
        "photo": "2.jpg"
    },
    {
        "id": "5",
        "desc": "Aladdin",
        "price": 5.0,
        "quantity": 9,
        "genre": "Dibujos",
        "photo": "5.jpg"
    },
    {
        "id": "1",
        "desc": "Titanic",
        "price": 5.0,
        "quantity": 0,
        "genre": "Melodrama",
        "photo": "1.jpg"
    },
    {
        "id": "4",
        "desc": "Akira",
        "price": 5.0,
        "quantity": 3,
        "genre": "Anime violento",
        "photo": "4.jpg"
    },
    {
        "id": "3",
        "desc": "Pulp Fiction",
        "price": 5.0,
        "quantity": 4,
        "genre": "Thriller violento",
        "photo": "3.jpg"
    }
]
);